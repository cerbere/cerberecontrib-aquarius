"""

Test class for cerbere Aquarius JPL L2

:copyright: Copyright 2017 Ifremer / Cersat.
:license: Released under GPL v3 license, see :ref:`license`.

.. sectionauthor:: Jeff Piolle <jfpiolle@ifremer.fr>
.. codeauthor:: Jeff Piolle <jfpiolle@ifremer.fr>
"""
import unittest

from cerbere.mapper.checker import Checker


class AquariusJPLNCFileChecker(Checker, unittest.TestCase):
    """Test class for Aquarius JPL L2 files"""

    def __init__(self, methodName="runTest"):
        super(AquariusJPLNCFileChecker, self).__init__(methodName)

    @classmethod
    def mapper(cls):
        """Return the mapper class name"""
        return 'aquariusjplncfile.AquariusJPLNCFile'

    @classmethod
    def datamodel(cls):
        """Return the related datamodel class name"""
        return 'Swath'

    @classmethod
    def test_file(cls):
        """Return the name of the test file for this test"""
        return "Q2015155144300.L2_SCI_V4.0.cap"

    @classmethod
    def download_url(cls):
        """Return the URL of the data test repository where to get the test
        files
        """
        return "ftp://ftp.ifremer.fr/ifremer/cersat/projects/cerbere/test_data/"
