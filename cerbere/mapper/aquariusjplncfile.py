# encoding: utf-8
"""
cerbere.mapper.AquariusJPLNCFile
================================

Mapper class for JPL AQUARIUS L2 CAP NetCDF files

:copyright: Copyright 2017 Ifremer / Cersat.
:license: Released under GPL v3 license, see :ref:`license`.

.. sectionauthor:: Jeff Piolle <jfpiolle@ifremer.fr>
.. codeauthor:: Jeff Piolle <jfpiolle@ifremer.fr>
"""
import os
from datetime import datetime

import numpy
from netCDF4 import date2num, num2date

import cerbere.mapper.slices
from cerbere.mapper.ncfile import NCFile
from cerbere.datamodel import field
from cerbere.datamodel import variable
from cerbere.mapper.abstractmapper import READ_ONLY, DEFAULT_TIME_UNITS


class AquariusJPLNCFile(NCFile):
    """Mapper class for JPL AQUARIUS L2 CAP NetCDF files"""

    def __init__(self,
                 url=None,
                 mode=READ_ONLY,
                 **kwargs):
        """
        Args:

        """
        geodim_matching = {
            'row': 'phony_dim_0',
            'cell': 'phony_dim_1'
        }
        geofield_matching = {
            'lat': 'beam_clat',
            'lon': 'beam_clon'
        }
        NCFile.__init__(self, url=url, mode=mode,
                        geodim_matching=geodim_matching,
                        geofield_matching=geofield_matching,
                        **kwargs)
        # there is no time reference in the file, we need to extract it from
        # the filename (ex: Q2015155144300.L2_SCI_V4.0.cap)
        self.start_time = date2num(
            datetime.strptime(
                os.path.basename(url)[1:14],
                '%Y%j%H%M%S'
            ),
            DEFAULT_TIME_UNITS
        )
        self.reference_time = date2num(
            datetime.strptime(
                os.path.basename(url)[1:8],
                '%Y%j'
            ),
            DEFAULT_TIME_UNITS
        )

    def read_field(self, fieldname):
        """
        Return the :class:`cerbere.field.Field` object corresponding to
        the requested fieldname.

        The :class:`cerbere.field.Field` class contains all the metadata
        describing a field (equivalent to a variable in netCDF).

        Args:
            fieldname (str): name of the field

        Returns:
            :class:`cerbere.field.Field`: the corresponding field object
        """
        if fieldname == 'time':
            # create a field for time
            vartime = variable.Variable(
                shortname='time',
                description='acquisition time',
                authority=self.get_naming_authority(),
                standardname='time'
            )
            fieldobj = field.Field(
                vartime,
                self.get_full_dimensions('lat'),
                datatype=numpy.dtype(numpy.int64),
                units=DEFAULT_TIME_UNITS
            )
            fieldobj.attach_storage(self.get_field_handler(fieldname))

        else:
            fieldobj = NCFile.read_field(self, fieldname)

        return fieldobj

    def read_values(self, fieldname, slices=None):
        """Read the data of a field.

        Args:
            fieldname (str): name of the field which to read the data from

            slices (list of slice, optional): list of slices for the field if
                subsetting is requested. A slice must then be provided for each
                field dimension. The slices are relative to the opened view
                (see :func:open) if a view was set when opening the file.

        Return:
            MaskedArray: array of data read. Array type is the same as the
                storage type.
        """
        if fieldname == 'time':
            # adjust slice with respect to view
            # time is only defined along-track
            dims = self.get_full_dimensions('lat').keys()
            dimsizes = self.get_full_dimensions('lat').values()
            newslices = cerbere.mapper.slices.get_absolute_slices(
                view=self.view,
                slices=slices,
                dimnames=dims,
                dimsizes=dimsizes
            )
            shape = cerbere.mapper.slices.get_shape_from_slice(newslices)
            newslices = [newslices[0]]

            total_seconds = NCFile.read_values(
                self,
                'sec',
                newslices) + self.reference_time

            # reshape to (row, cell) array
            total_seconds = numpy.resize(total_seconds,
                                         (shape[1], shape[0])
                                         ).transpose()
            return total_seconds
        else:
            return NCFile.read_values(
                self, fieldname, slices
            )

    def get_product_version(self):
        """return the product version"""
        return self.get_collection_id.split('_')[-1].strip('V')

    def get_collection_id(self):
        filename = os.path.basename(self.url).strip('.cap')
        return filename[filename.index('.'):]

    def get_start_time(self):
        """Returns the minimum date of the file temporal coverage.

        Returns:
            datetime: start time of the data in file.
        """
        return num2date(self.start_time, DEFAULT_TIME_UNITS)

    def get_end_time(self):
        """Returns the minimum date of the file temporal coverage.

        Returns:
            datetime: start time of the data in file.
        """
        return num2date(self.read_values('time').max(), DEFAULT_TIME_UNITS)