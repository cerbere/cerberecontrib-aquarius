# -*- coding: utf-8 -*-
from setuptools import setup, find_packages


requires = ['cerbere']

setup(
    name='cerberecontrib-aquarius',
    version='1.0',
    url='',
    download_url='',
    license='GPLv3',
    author='Jeff Piolle',
    author_email='jfpiolle@gmail.com',
    description='Cerbere extension for Aquarius formats',
    long_description="This package contains the Cerbere extension for Aquarius formats.",
    zip_safe=False,
    classifiers=[
        'Development Status :: 4 - Beta',
        'Environment :: Console',
        'Environment :: Web Environment',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: GPLv3 License',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Topic :: Documentation',
        'Topic :: Utilities',
    ],
    platforms='any',
    packages=find_packages(),
    include_package_data=True,
    package_data={},
    install_requires=requires,
    namespace_packages=['cerbere'],
)
